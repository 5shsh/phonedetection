# phonedetection #

[![](https://images.microbadger.com/badges/image/homesmarthome/phonedetection.svg)](https://microbadger.com/images/homesmarthome/phonedetection "Get your own image badge on microbadger.com")

[![](https://images.microbadger.com/badges/version/homesmarthome/phonedetection.svg)](https://microbadger.com/images/homesmarthome/phonedetection "Get your own version badge on microbadger.com")

Sends a MQTT message if a phone with the given IP and MAC address is detected in the appropriate network. After a device has been detected the container will sleep for 120 secs before it terminates (and automatically restarts) , otherwise it sleeps 60 secs before the next scan is started.

## Run
```
docker run -d \
  --restart=unless-stopped \
  --name=phonedetection_device_x \
  -e MQTT_HOST=10.1.2.3 \
  -e IP=10.1.2.3.4 \
  -e MAC=aa:bb:cc:dd:ee:ff \
  --net host \
  homesmarthome/phonedetection:latest
```