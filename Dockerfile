FROM resin/rpi-raspbian:jessie

MAINTAINER Jan Burkl <box@5square.de>

RUN apt-get update && \
    apt-get -y autoremove && \
    apt-get clean && \
    apt-get install -y \
    hping3 \
    arp-scan \
    mosquitto-clients \
    && rm -rf /var/lib/apt/lists/*

COPY ./scan.sh /scan.sh
RUN chmod a+x /scan.sh
ENTRYPOINT /scan.sh

#--------------------------------------------------------------------------------
# Labelling
#--------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION

LABEL de.5square.homesmarthome.build-date=$BUILD_DATE \
      de.5square.homesmarthome.name="homesmarthome/phonedetection" \
      de.5square.homesmarthome.description="detect phones in WIFI by using hping3 and arp-scan" \
      de.5square.homesmarthome.url="homesmarthome.5square.de" \
      de.5square.homesmarthome.vcs-ref=$VCS_REF \
      de.5square.homesmarthome.vcs-url="$VCS_URL" \
      de.5square.homesmarthome.vendor="5square" \
      de.5square.homesmarthome.version=$VERSION \
      de.5square.homesmarthome.schema-version="1.0"
