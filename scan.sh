#!/bin/bash

# v1.1 2017-04-11: VARIATION WITH LOGGING INCLUDED

# detect iphone by IP and MAC address.
# use MAC address too, to prevent false positives if IP might change
# return ON or OFF so output can be directly bound to a switch item


# number of retries, less is faster, but less accurate
MAXRETRIES=20

COUNT=0
while [ ${COUNT} -lt ${MAXRETRIES} ];
do

  sudo hping3 -q -2 -c 10 -p 5353 -i u1 ${IP} >/dev/null 2>&1
  sleep .1

  # Only arp specific device, grep for a mac-address
  STATUS=`arp-scan ${IP} | awk '{print $2}' | grep "${MAC}"`

  if [ ${#STATUS} -eq 17 ]; then
      # exit when phone is detected

      echo "ON"
      mosquitto_pub -h $MQTT_HOST -t "/phonedetection/sensor" -m '{"mac":"'"$MAC"'", "ip":"'"$IP"'", "status":"ON"}'
      sleep 300
      exit 0
  fi
  let COUNT=COUNT+1
  sleep .1
done

# consider away if reached max retries 
# echo `date '+%F %T'` - Phone not detected >> ${LOGFILE}
echo "OFF"

sleep 120

